package com.example.projecthub

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth


class MainActivity : AppCompatActivity() {

    lateinit var buttonNavigationButton: BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val homeFragment = FragmentHome()
        val categoryrgment = FragmentCategory()
        val profileFrgments = Fragment_Profile()
        buttonNavigationButton = findViewById(R.id.bottomNavigationView)
        setCurrentFragment(homeFragment)

        buttonNavigationButton.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.homeFragment -> setCurrentFragment(homeFragment)
                R.id.categoryFragment -> setCurrentFragment(categoryrgment)
                R.id.mPerson -> setCurrentFragment(profileFrgments)
            }
            true
        }

    }
    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flFragment, fragment)
            commit()
        }
}