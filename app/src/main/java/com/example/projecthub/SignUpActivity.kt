package com.example.projecthub


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase


class SignUpActivity : AppCompatActivity() {
    private val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
    lateinit var etEmail: EditText
    lateinit var userName: EditText
    private lateinit var etPass: EditText
    private lateinit var btnSignUp: Button
    var email = ""
    var name = ""

    //create Firebase authentication object
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        //View Bindings
        etEmail = findViewById(R.id.email1)
        etPass = findViewById(R.id.input)
        btnSignUp = findViewById(R.id.button_submit_1)
        userName = findViewById(R.id.nameTxt)

        //Initialising auth object
        auth = Firebase.auth
        btnSignUp.setOnClickListener {
            signUpUser()
        }
    }
    private fun signUpUser() {
         email = etEmail.text.toString()
        val pass = etPass.text.toString()
        val name = userName.text.toString()

        // check pass And email with length
        if (name.isBlank()) {
            Toast.makeText(this, "Please Enter  Your Name", Toast.LENGTH_SHORT).show()
            return
        }
        if (email.isBlank()) {
            Toast.makeText(this, "Please Enter Your Email", Toast.LENGTH_SHORT).show()
            return
        }
        if (pass.isBlank()) {
            Toast.makeText(this, "Please enter Your password ", Toast.LENGTH_SHORT).show()
        }
        if (email.matches(emailPattern.toRegex())) {
        } else {
            Toast.makeText(applicationContext, "Invalid email address", Toast.LENGTH_SHORT).show()
        }
        if (pass.length < 8) {
            Toast.makeText(this, "Password can't be 8 Special Characters", Toast.LENGTH_SHORT)
                .show()
            return
        }
        auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this) {
            if (it.isSuccessful) {
                Toast.makeText(this, "Successfully Singed Up", Toast.LENGTH_SHORT).show()
                saveFireStore()
                finish()
            }
        }.addOnFailureListener {
            Toast.makeText(this, "failed user", Toast.LENGTH_SHORT).show()
        }

    }
    fun saveFireStore() {
        val db = FirebaseFirestore.getInstance()
        val users:MutableMap<String,Any> = HashMap()
        users["Email"] = email
        users["First Name"] = name
        db.collection("users")
            .add(users)
            .addOnSuccessListener {

            }

    }

}