package com.example.projecthub

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.view.LayoutInflater


open class LoaderActivity: Activity() {
    private lateinit var dialog: AlertDialog
    val intentFlag = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
    fun showLoaderActivity() {
        val builder= AlertDialog.Builder(this)
        val dialogView= LayoutInflater.from(this).inflate(R.layout.progress_bar_layout,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        dialog = builder.create()
        dialog.show()
    }
    fun dismissLoaderActivity() {
        if (this::dialog.isInitialized){
            dialog.dismiss()
        }
    }

    fun isOnline():Boolean{
        val conManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val internetInfo =conManager.activeNetworkInfo
        return internetInfo!=null && internetInfo.isConnected
    }
 }
