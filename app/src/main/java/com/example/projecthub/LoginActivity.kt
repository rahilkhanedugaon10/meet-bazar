package com.example.projecthub

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : LoaderActivity() {
    private lateinit var signUp: TextView
    lateinit var etEmail: EditText
    private lateinit var etPass: EditText
    lateinit var button_submit: Button
    lateinit var auth: FirebaseAuth
    var email = ""
    var password = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intentFlag =
            Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK

        setContentView(R.layout.activity_login)
        signUp = findViewById(R.id.signup_text)
        signUp.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }
        // View Binding
        etEmail = findViewById(R.id.userinput)
        etPass = findViewById(R.id.input)
        // initialising Firebase auth object
        auth = FirebaseAuth.getInstance()
        button_submit = findViewById(R.id.button_submit)
        button_submit.setOnClickListener {
            if (email.isNotBlank() || password.isNotBlank()) {
            } else {
                email = etEmail.text.toString().trim()
                password = etPass.text.toString().trim()
                when {
                    email == "" -> {
                        AlertDialog.Builder(this)
                            .setMessage("please Enter your Email")
                            .show()
                    }
                    password == "" -> {
                        AlertDialog.Builder(this)
                            .setMessage("please Enter your Password")
                            .show()
                    }
                    else -> {
                        if (isOnline()){
                            showLoaderActivity()
                            login()
                        }

                    }
                }

            }
        }

    }

    private fun login() {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) {
            if (it.isSuccessful) {
                dismissLoaderActivity()
                Toast.makeText(this, "Successfully LoggedIn", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }.addOnFailureListener {
            dismissLoaderActivity()
            Toast.makeText(this, "Invalid User", Toast.LENGTH_SHORT).show()
        }

    }

}